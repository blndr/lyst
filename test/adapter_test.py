import json
from unittest.mock import patch

from pytest import raises

from lyst.adapter import JsonFileProjectLoader, JsonFileProjectSaver, TerminalProjectView, \
    TerminalProjectController
from lyst.domain import Project, InvalidCommandError, UserAction


class JsonFileProjectLoaderTest:
    def setup_method(self):
        self.loader = JsonFileProjectLoader()

    def test_load_returns_a_project_given_a_json_string(self):
        # given
        raw_project = """
        {
            "themes": [
                {
                    "goal": "do this",
                    "ideas": [
                        {"description": "first task", "done": true},
                        {"description": "second task", "done": false}
                    ]
                },
                {
                    "goal": "do that",
                    "ideas": []
                }
            ]
        }
        """

        # when
        project = self.loader.load(raw_project)

        # then
        assert project.themes[0].goal == 'do this'
        assert project.themes[0].ideas[0].description == 'first task'
        assert project.themes[0].ideas[0].done
        assert project.themes[0].ideas[1].description == 'second task'
        assert not project.themes[0].ideas[1].done
        assert project.themes[1].goal == 'do that'
        assert not project.themes[1].ideas


class JsonFileProjectSaverTest:
    def setup_method(self):
        self.saver = JsonFileProjectSaver()

    def test_dump_transforms_a_project_into_a_json_string(self):
        # given
        project = Project()
        project.create_theme('do this')
        project.current_theme.enrich('first task')
        project.current_theme.enrich('second task')
        project.current_theme.finish(0)
        project.create_theme('do that')

        # when
        raw_project = self.saver.dump(project)

        # then
        assert json.loads(raw_project) == json.loads("""
        {
            "themes": [
                {
                    "goal": "do this",
                    "ideas": [
                        {"description": "first task", "done": true},
                        {"description": "second task", "done": false}
                    ]
                },
                {
                    "goal": "do that",
                    "ideas": []
                }
            ]
        }
        """)


class TerminalProjectViewTest:
    def setup_method(self):
        self.view = TerminalProjectView()

    @patch('builtins.print')
    def test_should_print_a_project_view(self, print_mock):
        # given
        project = Project()
        project.create_theme('do this')
        project.current_theme.enrich('first task')
        project.current_theme.enrich('second task')
        project.current_theme.finish(0)
        project.create_theme('do that')

        # when
        self.view.display(project)

        # then
        print_mock.assert_called_with(
            '\u2217 do this\n'
            '\t\u2713 first task\n'
            '\t\u22C5 second task\n'
            '\033[1m\u2205 do that\033[0m\n'
        )


class TerminalProjectControllerTest:
    def setup_method(self):
        self.controller = TerminalProjectController()
        self.project = Project()
        self.project.create_theme('do this')
        self.project.current_theme.enrich('first task')
        self.project.current_theme.enrich('second task')

    def test_parsing_returns_an_user_command_if_successful(self):
        # given
        command = '+ do this'

        # when
        command = self.controller.parse_command(self.project, command)

        # then
        assert command.user_action == UserAction.ADD_IDEA
        assert command.content == 'do this'

    def test_raises_an_error_during_parsing_when_adding_idea_with_no_description(self):
        # given
        command = '+    '

        # when
        with raises(InvalidCommandError) as e:
            self.controller.parse_command(self.project, command)

        # then
        assert e.value.message == 'Please provide a description'

    def test_raises_an_error_during_parsing_when_creating_a_theme_without_a_goal(self):
        # given
        command = '>    '

        # when
        with raises(InvalidCommandError) as e:
            self.controller.parse_command(self.project, command)

        # then
        assert e.value.message == 'Please provide a goal'

    def test_raises_an_error_during_parsing_when_finishing_an_idea_with_word(self):
        # given
        command = '! abcd'

        # when
        with raises(InvalidCommandError) as e:
            self.controller.parse_command(self.project, command)

        # then
        assert e.value.message == 'Please provide a valid index'

    def test_raises_an_error_during_parsing_when_finishing_an_idea_with_negative_index(self):
        # given
        command = '! -1'

        # when
        with raises(InvalidCommandError) as e:
            self.controller.parse_command(self.project, command)

        # then
        assert e.value.message == 'Please provide a valid index'

    def test_raises_an_error_during_parsing_when_finishing_an_idea_with_too_large_index(self):
        # given
        command = '! 10'

        # when
        with raises(InvalidCommandError) as e:
            self.controller.parse_command(self.project, command)

        # then
        assert e.value.message == 'Please provide a valid index'

    def test_raises_an_error_during_parsing_when_switching_theme_with_word(self):
        # given
        command = '? abcd'

        # when
        with raises(InvalidCommandError) as e:
            self.controller.parse_command(self.project, command)

        # then
        assert e.value.message == 'Please provide a valid index'

    def test_raises_an_error_during_parsing_when_finishing_an_idea_with_negative_index(self):
        # given
        command = '? -1'

        # when
        with raises(InvalidCommandError) as e:
            self.controller.parse_command(self.project, command)

        # then
        assert e.value.message == 'Please provide a valid index'

    def test_raises_an_error_during_parsing_when_finishing_an_idea_with_too_large_index(self):
        # given
        command = '? 10'

        # when
        with raises(InvalidCommandError) as e:
            self.controller.parse_command(self.project, command)

        # then
        assert e.value.message == 'Please provide a valid index'

    @patch('builtins.input')
    def test_reads_an_add_idea_command(self, input_mock):
        # given
        input_mock.return_value = '+ reading stuff'

        # when
        command = self.controller.read_command()

        # then
        assert command == '+ reading stuff'

    @patch('builtins.input')
    def test_reads_a_finish_idea_command(self, input_mock):
        # given
        input_mock.return_value = '! 4'

        # when
        command = self.controller.read_command()

        # then
        assert command == '! 4'

    @patch('builtins.input')
    def test_reads_a_switch_theme_command(self, input_mock):
        # given
        input_mock.return_value = '? 3'

        # when
        command = self.controller.read_command()

        # then
        assert command == '? 3'

    @patch('builtins.input')
    def test_reads_a_create_theme_command(self, input_mock):
        # given
        input_mock.return_value = '> second task'

        # when
        command = self.controller.read_command()

        # then
        assert command == '> second task'
