#!/usr/bin/env python

from distutils.core import setup

setup(
    name='Lyst',
    version='0.1',
    description='',
    author='Abel André',
    author_email='aandre@octo.com',
    packages=['app'],
)