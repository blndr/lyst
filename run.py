import argparse
from os import path, system

from lyst.adapter import JsonFileProjectLoader, JsonFileProjectSaver, TerminalProjectView, \
    TerminalProjectController
from lyst.domain import Project, InvalidCommandError

parser = argparse.ArgumentParser()
parser.add_argument(
    '-F',
    '--project-file',
    help='The file containing the project as a JSON document'
)
args = parser.parse_args()

loader = JsonFileProjectLoader()
saver = JsonFileProjectSaver()
view = TerminalProjectView()
controller = TerminalProjectController()

if path.isfile(args.project_file):
    content = loader.read(args.project_file)
    project = loader.load(content)
else:
    project = Project()
    raw_project = saver.dump(project)
    saver.save(args.project_file, raw_project)

while True:
    view.display(project)
    user_command = controller.read_command()
    try:
        command = controller.parse_command(project, user_command)
    except InvalidCommandError as e:
        print('Error : %s' % e.message)
        continue
    controller.execute_command(project, command)
    system('clear')
    saver.save(args.project_file, saver.dump(project))
